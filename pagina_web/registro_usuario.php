<?php

session_start();



// Archivo de configuración de la base de datos

include 'configweb.php';



// Inicializa las variables de formulario

$email = $password = $confirm_password = '';

$mensaje = '';



if ($_SERVER["REQUEST_METHOD"] == "POST") {



    // Recoge los datos del formulario

    $email = $_POST["email"];

    $password = $_POST["password"];

    $confirm_password = $_POST["confirm_password"];



    // Conexión a la base de datos

    $conexion = mysqli_connect($servidor, $usuario, $clave, $bd, $puerto);



    // Verifica la conexión

    if (!$conexion) {

        $error = mysqli_connect_error();

        $n_error = mysqli_connect_errno();

        header("Location:errorweb.php?error=$error&n_error=$n_error");

    }



    // Escapa las variables por seguridad

    $email = mysqli_real_escape_string($conexion, $email);

    $password = mysqli_real_escape_string($conexion, $password);



    // Consulta SQL para verificar si el usuario ya está registrado

    $consulta = "SELECT * FROM Usuario WHERE email_usuario='$email' AND clave='$password'";



    // Ejecuta la consulta

    $resultado = mysqli_query($conexion, $consulta);



    // Verifica si el usuario ya está registrado

    if (mysqli_num_rows($resultado) > 0) {

        $mensaje = "El usuario ya está registrado.";

    } elseif ($password !== $confirm_password) {

        $mensaje = "Las contraseñas no coinciden.";

    } else {

        // Inserta el nuevo usuario en la base de datos

        $consulta_insertar = "INSERT INTO Usuario (email_usuario, clave) VALUES ('$email', '$password')";

        if (mysqli_query($conexion, $consulta_insertar)) {

            $mensaje = "Usuario registrado :)";

        } else {

            $mensaje = "Error al registrar usuario: " . mysqli_error($conexion);

        }

    }



    // Cierra la conexión

    mysqli_close($conexion);

}

?>



<!DOCTYPE html>

<html lang="es">

<head>

    <meta charset="UTF-8">

    <title>Registro de Usuarios</title>

    <style>

        .mensaje {

            font-size: 24px;

            font-weight: bold;

            color: red;

        }

    </style>

</head>

<body>



<h2>Registro de Usuarios</h2>



<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

    <label for="email">Correo Electrónico:</label><br>

    <input type="email" id="email" name="email" value="<?php echo $email; ?>" required><br>



    <label for="password">Contraseña:</label><br>

    <input type="password" id="password" name="password" required><br>



    <label for="confirm_password">Confirmar Contraseña:</label><br>

    <input type="password" id="confirm_password" name="confirm_password" required><br>



    <input type="submit" value="Registrarse">

</form>



<?php

// Muestra el mensaje con un tamaño de letra más grande

if (!empty($mensaje)) {

    echo '<p class="mensaje">' . $mensaje . '</p>';

}



// Si el usuario está registrado, muestra el enlace para volver a index.php

if ($mensaje === "Usuario registrado :)" || $mensaje === "El usuario ya está registrado.") {

    echo '<p><a href="index.php">Volver a Inicio</a></p>';

}

?>



</body>

</html>