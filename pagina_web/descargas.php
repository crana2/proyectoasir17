<?php



session_start();



if (!isset($_SESSION['logged']) || $_SESSION['logged'] !== true) {

    header("Location: index.php");

    exit;

}



include 'configweb.php';



$conexion = mysqli_connect($servidor, $usuario, $clave, $bd, $puerto);



if (!$conexion) {

    $error = mysqli_connect_error();

    $n_error = mysqli_connect_errno();

    header("Location:errorweb.php?error=$error&n_error=$n_error");

    exit;

}



if (isset($_GET['modelo'])) {

    if(isset($_SESSION['email'])){

        $email_usuario = $_SESSION['email'];

    } else {

        // Manejar el caso en que el correo electrónico del usuario no esté disponible en la sesión

        echo "Error: No se puede obtener el correo electrónico del usuario.";

        exit;

    }

}



?>



<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Descargas de Modelos 3D</title>

    <script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>

    <style>

        /* Estilos CSS */

        .container {

            display: flex;

            flex-wrap: wrap;

            justify-content: space-around;

        }



        .model-container {

            width: calc(33.33% - 20px);

            margin: 10px;

            border: 1px solid #ccc;

            padding: 10px;

            box-sizing: border-box;

        }



        .model-container img,

        .model-container model-viewer {

            max-width: 100%;

            height: auto;

            margin-bottom: 10px;

        }



        .download-link {

            text-align: center;

            display: block;

        }

    </style>

</head>

<body>

    <h1>Descargas de Modelos 3D</h1>

    <div class="container">

        <?php

        $sql = "SELECT id_modelo, ruta_modelo_3d, ruta_zip_descarga FROM Modelo";

        $result = mysqli_query($conexion, $sql);

        if (mysqli_num_rows($result) > 0) {

            while ($row = mysqli_fetch_assoc($result)) {

                echo "<div class='model-container'>";

                echo "<model-viewer src='" . $row['ruta_modelo_3d'] . "' camera-controls auto-rotate disable-zoom alt='Modelo 3D' style='width: 100%; height: 300px;'></model-viewer>";

                echo "<a class='download-link' href='actualizar_descargas.php?modelo=" . $row['id_modelo'] . "' download>Descargar Modelo</a>";

                echo "</div>";

            }

        } else {

            echo "No se encontraron modelos en la base de datos.";

        }

        mysqli_close($conexion);

        ?>

    </div>

</body>

</html>